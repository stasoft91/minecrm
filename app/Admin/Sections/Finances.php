<?php

namespace App\Admin\Sections;

use App\Client;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;

/**
 * Class Finances
 *
 * @property \App\Finance $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class Finances extends Section
{
    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title = 'Финансы';

    /**
     * @var string
     */
    protected $alias;

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        $table = \AdminDisplay::datatablesAsync()
            ->setFilters(
                \AdminDisplayFilter::field('client_id')->setTitle('ID Клиента [:value]')
            )
            ->with('client')
            ->setHtmlAttribute('class', 'table-primary')
            ->setColumns([
                \AdminColumn::text('client.id', 'ID Клиента'),
                \AdminColumn::text('client.name', 'Клиент'),
                \AdminColumn::text('action', 'Действие'),
                \AdminColumn::text('value', 'Значение'),
                \AdminColumn::datetime('date', 'Дата действия')->setFormat('d/m/Y'),
            ])->paginate(20);

        return $table;
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        return \AdminForm::panel()->addBody([
            \AdminFormElement::select('client_id', 'Клиент', Client::class)->setDisplay('name')->required(),
            \AdminFormElement::select('action', 'Действие')->setOptions([
                '=' => 'Первоначальный вклад',
                '-' => 'Вычет из вклада',
                ])->required(),
            \AdminFormElement::number('value', 'Значение')->required(),
            \AdminFormElement::date('date', 'Дата действия')->required(),

        ]);
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // remove if unused
    }
}
