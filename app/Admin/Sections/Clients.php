<?php

namespace App\Admin\Sections;

use App\MachineGroup;
use Illuminate\Database\Eloquent\Model;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Display\Extension\FilterInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;
use AdminDisplay;
use AdminColumn;
use AdminForm;
use AdminFormElement;
use AdminColumnFilter;

/**
 * Class Clients
 *
 * @property \App\Client $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class Clients extends Section
{
    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title = 'Клиенты';

    /**
     * @var string
     */
    protected $alias;

    /**
     * @return DisplayInterface
     * @throws
     */
    public function onDisplay()
    {
        $display = AdminDisplay::datatablesAsync();

        return $display;
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        return AdminForm::panel()->addBody([
            AdminFormElement::text('name', 'Имя')->required(),
            AdminFormElement::text('phone', 'Телефон'),
            AdminFormElement::text('email', 'Email'),
            AdminFormElement::text('info', 'Дополнительно'),

            AdminFormElement::file('contract', 'Договор')->setUploadPath(function(\Illuminate\Http\UploadedFile $file) {
                $path = 'uploads/contracts/';

                if (!is_dir(public_path($path)))
                    mkdir(public_path($path));

                return $path;
            }),
            AdminFormElement::image('photo', 'Фотография')->setUploadPath(function(\Illuminate\Http\UploadedFile $file) {
                $path = 'uploads/photos/';

                if (!is_dir(public_path($path)))
                    mkdir(public_path($path ));

                return $path;
            }),

            AdminFormElement::multiselect('machine_groups', 'Группа машин клиента', MachineGroup::class)->setDisplay('name'),
        ]);
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        //
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // remove if unused
    }

    public function isDeletable(Model $model)
    {
        return false;
    }
}
