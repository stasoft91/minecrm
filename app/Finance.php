<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Finance extends Model
{
    public function client()
    {
        return $this->belongsTo(Client::class);
    }
}
