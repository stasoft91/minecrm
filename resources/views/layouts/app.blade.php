<!DOCTYPE html>
<html lang="en">
<head>
    <title>Панель управления</title>
    <meta charset="utf-8" />
    <meta content="{{csrf_token()}}" name="csrf-token" />
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="IE=edge" http-equiv="X-UA-Compatible" />
    <script>window.GlobalConfig = {"debug":true,"env":"local","locale":"ru","url":"http:\/\/minecrm","lang":{"dashboard":"\u041f\u0430\u043d\u0435\u043b\u044c","404":"\u0421\u0442\u0440\u0430\u043d\u0438\u0446\u0430 \u043d\u0435 \u043d\u0430\u0439\u0434\u0435\u043d\u0430.","auth":{"title":"\u0410\u0432\u0442\u043e\u0440\u0438\u0437\u0430\u0446\u0438\u044f","username":"\u041b\u043e\u0433\u0438\u043d","password":"\u041f\u0430\u0440\u043e\u043b\u044c","login":"\u0412\u043e\u0439\u0442\u0438","logout":"\u0412\u044b\u0439\u0442\u0438","wrong-username":"\u041d\u0435\u0432\u0435\u0440\u043d\u044b\u0439 \u043b\u043e\u0433\u0438\u043d","wrong-password":"\u0438\u043b\u0438 \u043f\u0430\u0440\u043e\u043b\u044c","since":"\u0417\u0430\u0440\u0435\u0433\u0438\u0441\u0442\u0440\u0438\u0440\u043e\u0432\u0430\u043d :date"},"model":{"create":"\u0421\u043e\u0437\u0434\u0430\u043d\u0438\u0435 \u0434\u043e\u043a\u0443\u043c\u0435\u043d\u0442\u0430 \u0432 \u0440\u0430\u0437\u0434\u0435\u043b\u0435 :title","edit":"\u0420\u0435\u0434\u0430\u043a\u0442\u0438\u0440\u043e\u0432\u0430\u043d\u0438\u0435 \u0437\u0430\u043f\u0438\u0441\u0438 \u0432 \u0440\u0430\u0437\u0434\u0435\u043b\u0435 :title"},"links":{"index_page":"\u041d\u0430 \u0441\u0430\u0439\u0442"},"ckeditor":{"upload":{"success":"\u0424\u0430\u0439\u043b \u0431\u044b\u043b \u0443\u0441\u043f\u0435\u0448\u043d\u043e \u0437\u0430\u0433\u0440\u0443\u0436\u0435\u043d: \\n- \u0420\u0430\u0437\u043c\u0435\u0440: :size \u043a\u0431 \\n- \u0448\u0438\u0440\u0438\u043d\u0430\/\u0432\u044b\u0441\u043e\u0442\u0430: :width x :height","error":{"common":"\u0412\u043e\u0437\u043d\u0438\u043a\u043b\u0430 \u043e\u0448\u0438\u0431\u043a\u0430 \u043f\u0440\u0438 \u0437\u0430\u0433\u0440\u0443\u0437\u043a\u0435 \u0444\u0430\u0439\u043b\u0430.","wrong_extension":"\u0424\u0430\u0439\u043b \":file\" \u0438\u043c\u0435\u0435\u0442 \u043d\u0435\u0432\u0435\u0440\u043d\u044b\u0439 \u0442\u0438\u043f.","filesize_limit":"\u041c\u0430\u043a\u0441\u0438\u043c\u0430\u043b\u044c\u043d\u044b\u0439 \u0440\u0430\u0437\u043c\u0435\u0440 \u0444\u0430\u0439\u043b\u0430 :size \u043a\u0431.","imagesize_max_limit":"\u0428\u0438\u0440\u0438\u043d\u0430 x \u0412\u044b\u0441\u043e\u0442\u0430 = :width x :height \\n \u041c\u0430\u043a\u0441\u0438\u043c\u0430\u043b\u044c\u043d\u044b\u0439 \u0440\u0430\u0437\u043c\u0435\u0440 \u0438\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435 \u0434\u043e\u043b\u0436\u0435\u043d \u0431\u044b\u0442\u044c: :maxwidth x :maxheight","imagesize_min_limit":"\u0428\u0438\u0440\u0438\u043d\u0430 x \u0412\u044b\u0441\u043e\u0442\u0430 = :width x :height \\n \u041c\u0438\u043d\u0438\u043c\u0430\u043b\u044c\u043d\u044b\u0439 \u0440\u0430\u0437\u043c\u0435\u0440 \u0438\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435 \u0434\u043e\u043b\u0436\u0435\u043d \u0431\u044b\u0442\u044c: :minwidth x :minheight"}},"image_browser":{"title":"\u0412\u0441\u0442\u0430\u0432\u043a\u0430 \u0438\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u044f \u0441 \u0441\u0435\u0440\u0432\u0435\u0440\u0430","subtitle":"\u0412\u044b\u0431\u0435\u0440\u0438\u0442\u0435 \u0438\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435 \u0434\u043b\u044f \u0432\u0441\u0442\u0430\u0432\u043a\u0438"}},"table":{"no-action":"\u041d\u0435\u0442 \u0434\u0435\u0439\u0441\u0442\u0432\u0438\u044f","make-action":"\u041e\u0442\u043f\u0440\u0430\u0432\u0438\u0442\u044c","new-entry":"\u041d\u043e\u0432\u0430\u044f \u0437\u0430\u043f\u0438\u0441\u044c","edit":"\u0420\u0435\u0434\u0430\u043a\u0442\u0438\u0440\u043e\u0432\u0430\u0442\u044c","restore":"\u0412\u043e\u0441\u0441\u0442\u0430\u043d\u043e\u0432\u0438\u0442\u044c","delete":"\u0423\u0434\u0430\u043b\u0438\u0442\u044c","delete-confirm":"\u0412\u044b \u0443\u0432\u0435\u0440\u0435\u043d\u044b, \u0447\u0442\u043e \u0445\u043e\u0442\u0438\u0442\u0435 \u0443\u0434\u0430\u043b\u0438\u0442\u044c \u044d\u0442\u0443 \u0437\u0430\u043f\u0438\u0441\u044c?","action-confirm":"\u0412\u044b \u0443\u0432\u0435\u0440\u0435\u043d\u044b, \u0447\u0442\u043e \u0445\u043e\u0442\u0438\u0442\u0435 \u0441\u043e\u0432\u0435\u0440\u0448\u0438\u0442\u044c \u044d\u0442\u043e \u0434\u0435\u0439\u0441\u0442\u0432\u0438\u0435?","delete-error":"\u041d\u0435\u0432\u043e\u0437\u043c\u043e\u0436\u043d\u043e \u0443\u0434\u0430\u043b\u0438\u0442\u044c \u044d\u0442\u0443 \u0437\u0430\u043f\u0438\u0441\u044c. \u041d\u0435\u043e\u0431\u0445\u043e\u0434\u0438\u043c\u043e \u043f\u0440\u0435\u0434\u0432\u0430\u0440\u0438\u0442\u0435\u043b\u044c\u043d\u043e \u0443\u0434\u0430\u043b\u0438\u0442\u044c \u0432\u0441\u0435 \u0441\u0432\u044f\u0437\u0430\u043d\u043d\u044b\u0435 \u0437\u0430\u043f\u0438\u0441\u0438.","destroy":"\u0423\u0434\u0430\u043b\u0438\u0442\u044c","destroy-confirm":"\u0412\u044b \u0443\u0432\u0435\u0440\u0435\u043d\u044b, \u0447\u0442\u043e \u0445\u043e\u0442\u0438\u0442\u0435 \u0443\u0434\u0430\u043b\u0438\u0442\u044c \u044d\u0442\u0443 \u0437\u0430\u043f\u0438\u0441\u044c?","destroy-error":"\u041d\u0435\u0432\u043e\u0437\u043c\u043e\u0436\u043d\u043e \u0443\u0434\u0430\u043b\u0438\u0442\u044c \u044d\u0442\u0443 \u0437\u0430\u043f\u0438\u0441\u044c. \u041d\u0435\u043e\u0431\u0445\u043e\u0434\u0438\u043c\u043e \u043f\u0440\u0435\u0434\u0432\u0430\u0440\u0438\u0442\u0435\u043b\u044c\u043d\u043e \u0443\u0434\u0430\u043b\u0438\u0442\u044c \u0432\u0441\u0435 \u0441\u0432\u044f\u0437\u0430\u043d\u043d\u044b\u0435 \u0437\u0430\u043f\u0438\u0441\u0438.","moveUp":"\u041f\u043e\u0434\u0432\u0438\u043d\u0443\u0442\u044c \u0432\u0432\u0435\u0440\u0445","moveDown":"\u041f\u043e\u0434\u0432\u0438\u043d\u0443\u0442\u044c \u0432\u043d\u0438\u0437","error":"\u0412 \u043f\u0440\u043e\u0446\u0435\u0441\u0441\u0435 \u043e\u0431\u0440\u0430\u0431\u043e\u0442\u043a\u0438 \u0432\u0430\u0448\u0435\u0433\u043e \u0437\u0430\u043f\u0440\u043e\u0441\u0430 \u0432\u043e\u0437\u043d\u0438\u043a\u043b\u0430 \u043e\u0448\u0438\u0431\u043a\u0430","filter":"\u041f\u043e\u043a\u0430\u0437\u0430\u0442\u044c \u043f\u043e\u0434\u043e\u0431\u043d\u044b\u0435 \u0437\u0430\u043f\u0438\u0441\u0438","filter-goto":"\u041f\u0435\u0440\u0435\u0439\u0442\u0438","save":"\u0421\u043e\u0445\u0440\u0430\u043d\u0438\u0442\u044c","save_and_close":"\u0421\u043e\u0445\u0440\u0430\u043d\u0438\u0442\u044c \u0438 \u0437\u0430\u043a\u0440\u044b\u0442\u044c","save_and_create":"\u0421\u043e\u0445\u0440\u0430\u043d\u0438\u0442\u044c \u0438 \u0441\u043e\u0437\u0434\u0430\u0442\u044c","cancel":"\u041e\u0442\u043c\u0435\u043d\u0438\u0442\u044c","download":"\u0421\u043a\u0430\u0447\u0430\u0442\u044c","all":"\u0412\u0441\u0435","processing":"<i class=\"fa fa-5x fa-circle-o-notch fa-spin\"><\/i>","loadingRecords":"\u041f\u043e\u0434\u043e\u0436\u0434\u0438\u0442\u0435...","lengthMenu":"\u041e\u0442\u043e\u0431\u0440\u0430\u0436\u0430\u0442\u044c _MENU_ \u0437\u0430\u043f\u0438\u0441\u0435\u0439","zeroRecords":"\u041d\u0435 \u043d\u0430\u0439\u0434\u0435\u043d\u043e \u043f\u043e\u0434\u0445\u043e\u0434\u044f\u0449\u0438\u0445 \u0437\u0430\u043f\u0438\u0441\u0435\u0439.","info":"\u0417\u0430\u043f\u0438\u0441\u0438 \u0441 _START_ \u043f\u043e _END_ \u0438\u0437 _TOTAL_","infoEmpty":"\u0417\u0430\u043f\u0438\u0441\u0438 \u0441 0 \u043f\u043e 0 \u0438\u0437 0","infoFiltered":"(\u043e\u0442\u0444\u0438\u043b\u044c\u0442\u0440\u043e\u0432\u0430\u043d\u043e \u0438\u0437 _MAX_ \u0437\u0430\u043f\u0438\u0441\u0435\u0439)","infoThousands":"","infoPostFix":"","search":"\u041f\u043e\u0438\u0441\u043a: ","emptyTable":"\u041d\u0435\u0442 \u0437\u0430\u043f\u0438\u0441\u0435\u0439","paginate":{"first":"\u041f\u0435\u0440\u0432\u0430\u044f","previous":"&larr;","next":"&rarr;","last":"\u041f\u043e\u0441\u043b\u0435\u0434\u043d\u044f\u044f"},"filters":{"control":"\u0424\u0438\u043b\u044c\u0442\u0440"}},"tree":{"expand":"\u0420\u0430\u0437\u0432\u0435\u0440\u043d\u0443\u0442\u044c \u0432\u0441\u0435","collapse":"\u0421\u0432\u0435\u0440\u043d\u0443\u0442\u044c \u0432\u0441\u0435"},"editable":{"checkbox":{"checked":"\u0414\u0430","unchecked":"\u041d\u0435\u0442"}},"select":{"nothing":"\u041d\u0438\u0447\u0435\u0433\u043e \u043d\u0435 \u0432\u044b\u0431\u0440\u0430\u043d\u043e","selected":"\u0432\u044b\u0431\u0440\u0430\u043d\u043e","placeholder":"\u0412\u044b\u0431\u0435\u0440\u0438\u0442\u0435 \u0438\u0437 \u0441\u043f\u0438\u0441\u043a\u0430","no_items":"\u041d\u0435\u0442 \u044d\u043b\u0435\u043c\u0435\u043d\u0442\u043e\u0432","init":"\u041d\u0430\u0436\u043c\u0438\u0442\u0435 Enter \u0447\u0442\u043e \u0431\u044b \u0432\u044b\u0431\u0440\u0430\u0442\u044c","limit":"\u0438 \u0435\u0449\u0435 ${count}","deselect":"\u041d\u0430\u0436\u043c\u0438\u0442\u0435 Enter \u0447\u0442\u043e \u0431\u044b \u0441\u0431\u0440\u043e\u0441\u0438\u0442\u044c"},"image":{"browse":"\u0412\u044b\u0431\u043e\u0440 \u0438\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u044f","browseMultiple":"\u0412\u044b\u0431\u043e\u0440 \u0438\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0439","remove":"\u0423\u0434\u0430\u043b\u0438\u0442\u044c","removeMultiple":"\u0423\u0434\u0430\u043b\u0438\u0442\u044c"},"file":{"browse":"\u0412\u044b\u0431\u043e\u0440 \u0444\u0430\u0439\u043b\u0430","remove":"\u0423\u0434\u0430\u043b\u0438\u0442\u044c"},"button":{"yes":"\u0414\u0430","no":"\u041d\u0435\u0442","cancel":"\u041e\u0442\u043c\u0435\u043d\u0430"},"message":{"created":"<i class=\"fa fa-check fa-lg\"><\/i> \u0417\u0430\u043f\u0438\u0441\u044c \u0443\u0441\u043f\u0435\u0448\u043d\u043e \u0441\u043e\u0437\u0434\u0430\u043d\u0430","updated":"<i class=\"fa fa-check fa-lg\"><\/i> \u0417\u0430\u043f\u0438\u0441\u044c \u0443\u0441\u043f\u0435\u0448\u043d\u043e \u043e\u0431\u043d\u043e\u0432\u043b\u0435\u043d\u0430","deleted":"<i class=\"fa fa-check fa-lg\"><\/i> \u0417\u0430\u043f\u0438\u0441\u044c \u0443\u0441\u043f\u0435\u0448\u043d\u043e \u0443\u0434\u0430\u043b\u0435\u043d\u0430","restored":"<i class=\"fa fa-check fa-lg\"><\/i> \u0417\u0430\u043f\u0438\u0441\u044c \u0443\u0441\u043f\u0435\u0448\u043d\u043e \u0432\u043e\u0441\u0441\u0442\u0430\u043d\u043e\u0432\u043b\u0435\u043d\u0430","something_went_wrong":"\u0427\u0442\u043e-\u0442\u043e \u043f\u043e\u0448\u043b\u043e \u043d\u0435 \u0442\u0430\u043a!","are_you_sure":"\u0412\u044b \u0443\u0432\u0435\u0440\u0435\u043d\u044b?","access_denied":"\u0414\u043e\u0441\u0442\u0443\u043f \u0437\u0430\u043f\u0440\u0435\u0449\u0435\u043d","validation_error":"\u041e\u0448\u0438\u0431\u043a\u0430 \u0432\u0430\u043b\u0438\u0434\u0430\u0446\u0438\u0438"}},"wysiwyg":{"default":"ckeditor","ckeditor":{"height":200,"extraPlugins":"uploadimage,image2,justify,youtube,uploadfile"},"tinymce":{"height":200},"simplemde":{"hideIcons":["side-by-side","fullscreen"]}},"template":{"asset_dir":"packages\/sleepingowl\/default","view_namespace":"sleeping_owl::default","name":"AdminLTE 2","version":"2.3.8","homepage":"https:\/\/almsaeedstudio.com\/"},"user_id":1};</script>
    <link media="all" type="text/css" rel="stylesheet" href="/packages/sleepingowl/default/css/admin-app.css">
    <script></script>


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <style>
        [v-cloak] {
            display: none;
        }
    </style>
    @yield('css')

</head>
<body class="skin-blue sidebar-mini">
<div class="wrapper" id="vueApp">
    <header class="main-header">
        <a href="/admin" class="logo">
            <span class="logo-lg">Панель управления</span>
            <span class="logo-mini">Панель управления</span>
        </a>

        <nav class="navbar navbar-static-top" role="navigation">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                </ul>

                <ul class="nav navbar-nav navbar-right">
                </ul>
            </div>
        </nav>
    </header>

    <aside class="main-sidebar">
        <section class="sidebar">


            <ul class="sidebar-menu">

                <li>
                    <a href="/admin">
                        <i class="fa fa-dashboard"></i>
                        <span>Главная</span>

                    </a>
                </li>

                <li class="{{Request::segment(2) == 'clients' ? 'active' : ""}}">
                    <a href="/admin/clients#search=&page=1&per_page=50">
                        <i class="fa fa-users"></i>
                        <span>Клиенты</span>

                    </a>
                </li>

                <li  class="has-child">
                    <a href="#" >
                        <i class="fa fa-cog"></i>
                        <span>Настройки</span>
                        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>

                    </span>
                    </a>

                    <ul class="treeview-menu">
                        <li >
                            <a href="/admin/locations">
                                <i class="fa fa-industry"></i>
                                <span>Локации</span>

                            </a>
                        </li>

                        <li >
                            <a href="/admin/machine_groups">
                                <i class="fa fa-object-group"></i>
                                <span>Группы машин</span>

                            </a>
                        </li>

                        <li >
                            <a href="/admin/machines">
                                <i class="fa fa-server"></i>
                                <span>Машины</span>
                            </a>
                        </li>

                        <li >
                            <a href="/admin/finances">
                                <i class="fa fa-rub"></i>
                                <span>Финансы</span>
                            </a>
                        </li>

                    </ul>
                </li>

                <li  class="has-child">
                    <a href="#" >
                        <i class="fa fa-id-card"></i>
                        <span>Пользователи CRM</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>

                        </span>
                    </a>

                    <ul class="treeview-menu">
                        <li >
                            <a href="/admin/users">
                                <i class="fa fa-id-card"></i>
                                <span>Пользователи</span>

                            </a>
                        </li>

                        <li >
                            <a href="/admin/roles">
                                <i class="fa fa-id-badge"></i>
                                <span>Роли</span>

                            </a>
                        </li>

                    </ul>
                </li>

                <li>
                    <a href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                       document.getElementById('logout-form').submit();">
                        <i class="fa fa-sign-out"></i> Выход
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </li>

            </ul>

        </section>
    </aside>

    <div class="content-wrapper">


        <div class="content-header">
            <h1>
                @yield ('title')
            </h1>
        </div>

        <div class="content body">

            @yield('content')

        </div>
    </div>
</div>

<script src="/packages/sleepingowl/default/js/admin-app.js"></script>
<script src="/packages/sleepingowl/default/js/modules.js"></script>

@yield('js')

</body>
</html>
