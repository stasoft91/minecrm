<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Relations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('machines', function(Blueprint $table){
            $table->foreign('machine_group_id')->references('id')->on('machine_groups')
                ->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });

        Schema::table('machine_groups', function(Blueprint $table){
            $table->foreign('location_id')->references('id')->on('locations')
                ->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });

        Schema::table('client_machine_group', function (Blueprint $table) {
            $table->foreign('client_id')->references('id')->on('clients')
                ->onUpdate('RESTRICT')->onDelete('RESTRICT');

            $table->foreign('machine_group_id')->references('id')->on('machine_groups')
                ->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('machines', function(Blueprint $table){
            $table->dropForeign('machines_machine_group_id_foreign');
        });

        Schema::table('machine_groups', function(Blueprint $table){
            $table->dropForeign('machine_groups_location_id_foreign');
        });

        Schema::table('client_machine_group', function (Blueprint $table) {
            $table->dropForeign('client_machine_group_client_id_foreign');
            $table->dropForeign('client_machine_group_machine_group_id_foreign');
        });
    }
}
