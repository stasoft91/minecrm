<?php

namespace App\Http\Controllers;

use App\Client;
use App\MachineGroup;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class MachineGroupController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create (Request $request)
    {
        $mg = new MachineGroup();
        $mg->name = $request->name;
        $mg->location_id = $request->location_id;
        $mg->save();

        Client::find($request->client_id)->machine_groups()->attach($mg);

        return new JsonResponse('true', 200);
    }
}
