Vue.component('searchText', {
    template: '#vue-table2-search-text',

    data: function() {
        return {filterText: ''};
    },
    methods: {
        doFilter: function()
        {
            clients_table.searchText = this.filterText;

            clients_table.onChangePage(1);

            location.reload();
        },

        resetFilter: function () {
            this.filterText = '';
            clients_table.searchText = this.filterText;
        }
    }
});

