Vue.component('detailRow', {
    template: '#vue-table2-detail-row',

    props: {
        rowData: {
            type: Object,
            required: true
        },
        rowIndex: {
            type: Number
        }
    },
    methods: {
        sumFinances: function(finances) {
            var init_investment_obj = _.find(finances, function(o){ return o.action === '='});
            if (init_investment_obj === undefined)
                return 0;
            else
                init_investment = init_investment_obj.value;

            return (init_investment - _.sumBy(finances, function(o) { return o.action !== '=' ? o.value : 0; }));
        },

        getMachineTitle: function(machine) {

            str1 = '';
            str2 = '';


            if (machine.consumption > 0)
                str1 = machine.consumption + ' Вт';

            if (machine.price_per_month > 0)
                str2 = machine.price_per_month + ' р.';

            if (str1 && str2)
                result = str1 + ' ' + str2;
            else if ( !str1 && !str2 )
                result = 'Не заполнено';
            else
                result = str1 ? str1 : str2;

            return result;
        },

        onAddMachineGroupModalClick: function(row)
        {
            var name = translit(row.name);
            var nameArray = name.split('_');
            var resultName;

            if (nameArray.length === 2)
                resultName = nameArray[0][0].toUpperCase() + nameArray[1][0].toUpperCase();
            else
                resultName = nameArray[0][0].toUpperCase();

            this.$root.modals.machine_group.client_id = row.id;
            this.$root.modals.machine_group.name = resultName;
            $('#machine-group-add-modal').modal('show');

        },

        onAddMachineModalClick: function(machine_group)
        {
            Number.prototype.pad = function(size) {
                var s = String(this);
                while (s.length < (size || 2)) {s = "0" + s;}
                return s;
            }

            if (machine_group.machines.length > 0)
                machineNewNumber = 1 + parseInt(
                    _.maxBy(machine_group.machines, function(o) {
                        return parseInt(o.name.replace(/\D/g,''));
                    }).name.replace(/\D/g,'')
                );
            else
                machineNewNumber = 1;

            machineNewName = machine_group.name + machineNewNumber.pad(3);

            console.log(machine_group);

            this.$root.modals.machine.id = null;
            this.$root.modals.machine.machine_group_id = machine_group.id;
            this.$root.modals.machine.name = machineNewName;
            $('#machine-add-modal').modal('show');
        },

        onEditMachineModalClick: function(machine)
        {
            this.$root.modals.machine.id = machine.id;
            this.$root.modals.machine.name = machine.name;
            this.$root.modals.machine.consumption = machine.consumption;
            this.$root.modals.machine.price_per_month = machine.price_per_month;
            this.$root.modals.machine.note = machine.note;
            this.$root.modals.machine.date_start = machine.date_start;
            this.$root.modals.machine.machine_group_id = machine.machine_group_id;
            $('#machine-add-modal').modal('show');
        },

        onAddFinanceModalClick: function(client_id, sum)
        {
            this.$root.modals.finance = {
                action: sum == 0 ? '=' : '-',
                value: null,
                date: null,
                client_id: client_id,
                id: null,
                showValueError: false
            };

            $('#finance-add-modal').modal('show');

        },

        onEditFinanceModalClick: function(finance)
        {
            this.$root.modals.finance = finance;
            $('#finance-add-modal').modal('show');

        }
    },
});

