<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Machine extends Model
{
    protected $casts = [
        'date_start' => 'date'
    ];

    public function machine_group()
    {
        return $this->belongsTo(MachineGroup::class);
    }
}
