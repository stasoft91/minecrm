<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/admin');
});

Auth::routes();
Route::get('/logout', function()
{
    Auth::logout();
    return redirect('/');
});


Route::get('/home', 'HomeController@index')->name('home');

Route::get('/admin/clients', 'ClientsController@viewIndex');
Route::post('/admin/clients', 'ClientsController@generate');

Route::get('/api/client_list', 'ClientsController@apiClientList');
Route::post('/api/clients/{client_id}/delete', 'ClientsController@delete');

Route::post('/api/machine_group', 'MachineGroupController@create');
Route::post('/api/machine', 'MachineController@create');
Route::delete('/api/machine', 'MachineController@delete');

Route::post('/api/finance', 'FinanceController@create');
Route::delete('/api/finance', 'FinanceController@delete');