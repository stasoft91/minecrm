<?php

namespace App\Admin\Sections;

use App\MachineGroup;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;
use AdminDisplay;
use AdminColumn;
use AdminForm;
use AdminFormElement;

/**
 * Class Machines
 *
 * @property \App\Machine $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class Machines extends Section
{
    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title = 'Машины';

    /**
     * @var string
     */
    protected $alias;

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        return AdminDisplay::datatablesAsync()
            ->with('machine_group')
            ->setHtmlAttribute('class', 'table-primary')
            ->setColumns([
                AdminColumn::link('name', 'Название')->setWidth('250px'),
                AdminColumn::text('machine_group.name', 'Принадлежит к группе'),
                AdminColumn::text('consumption', 'Потребление Вт\ч'),
                AdminColumn::text('price_per_month', 'Цена размещения в месяц'),
                AdminColumn::datetime('date_start', 'Дата начала работы')->setFormat('d/m/Y'),
            ])->paginate(20);
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        return AdminForm::panel()->addBody([
            AdminFormElement::text('name', 'Название')->required()->unique(),
            AdminFormElement::select('machine_group_id', 'Принадлежит к группе', MachineGroup::class)->setDisplay('name'),
            AdminFormElement::number('consumption', 'Потребление Вт\ч')->setStep(1)->required(),
            AdminFormElement::number('price_per_month', 'Цена размещения в месяц')->setStep(1)->required(),
            AdminFormElement::text('note', 'Заметка'),

            AdminFormElement::date('date_start', 'Дата начала работы')->setCurrentDate()->required(),
        ]);
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // remove if unused
    }
}
