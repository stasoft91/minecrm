@extends('layouts.app')

@section('css')
    <link href="https://unpkg.com/flatpickr@4.2.4/dist/flatpickr.min.css" rel="stylesheet">

    <style>
        .vuetable-slot .table-name-container {
            cursor: pointer;
        }
        .right.aligned {
            cursor: pointer;
        }
    </style>
@endsection

@section('title')
    Клиенты
@endsection

@section('content')
<div id="places_list" v-cloak>
    <div class="row">
        <div class="col-md-2 col-sm-2 col-xs-2" v-for="(consumption, location) in locationConsumption">
            <div class="info-box">
                <span class="info-box-icon bg-aqua"><i class="fa fa-industry"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">@{{ location }}</span>
                    <span class="info-box-number">@{{ consumption/1000 }} кВт</span>
                </div>
                <!-- /.info-box-content -->
            </div>
        </div>
    </div>
    <div class="box">
        <div class="box-header">
            <h3 class="box-title pull-left">Список клиентов</h3>
            <search-text ref="searchtext"></search-text>
            <br>
            <br>
            <a  href="/admin/clients/create"
                class="btn btn-primary">

                <i class="fa fa-plus"></i> Добавить клиента
            </a>

            &nbsp;

            <div class="btn-group">
                <button class="btn btn-default"
                        @click.prevent="onGenerateReport">
                    <i class="fa fa-pie-chart"></i>
                    Сформировать отчёт <span v-if="$refs.vuetable && $refs.vuetable.selectedTo.length > 0">(@{{ $refs.vuetable.selectedTo.length }})</span>
                    <br>
                    <small>
                        c <span v-text="datePeriodStart ? datePeriodStart : 'начала месяца'"></span>
                        по <span v-text="datePeriodEnd ? datePeriodEnd : 'сегодня'"></span>
                    </small>
                </button>
                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <span class="caret"></span>
                    <span class="sr-only">Меню</span>
                    <br>
                    &nbsp;
                </button>
                <ul class="dropdown-menu" role="menu">
                    <li>
                        <flat-pickr v-model="datePeriodStart" :config="dateConfigStart" placeholder="Выбрать начало периода..."></flat-pickr>
                        <flat-pickr v-model="datePeriodEnd" :config="dateConfigEnd" placeholder="Выбрать конец периода..."></flat-pickr>
                    </li>
                </ul>
            </div>

        </div><!-- /.box-header -->

        <div class="box-body">
            <vuetable ref="vuetable"
                      :per-page="perPage"
                      :api-url="'/api/client_list?'+filter"
                      :fields="fields"
                      track-by="id"
                      pagination-path=""
                      :css="css.table"
                      detail-row-component="detailRow"
                      @vuetable:cell-clicked="onCellClicked"
                      @vuetable:pagination-data="onPaginationData">

                <template slot="actions" slot-scope="props">
                    <div class="table-button-container">
                        <button class="btn btn-default" @click="editRow(props.rowData)">
                            <i class="fa fa-edit"></i> Редактировать
                        </button>&nbsp;&nbsp;
                        <button class="btn btn-danger" @click="deleteRow(props.rowData)">
                            <i class="fa fa-times"></i> Удалить
                        </button>&nbsp;&nbsp;
                    </div>
                </template>

                <template slot="name" slot-scope="props">
                    <div class="table-name-container" @click="onCellClicked(props.rowData, props.rowData)">
                        <a :href="'/admin/clients/'+props.rowData.id+'/edit'">
                            @{{ props.rowData.name }}
                        </a>
                    </div>
                </template>

            </vuetable>

            <div class="row">
                <div class="col-md-6">
                    <vuetable-pagination-info
                            ref="paginationInfo"
                            :info-template="'Записи с {from} по {to} из {total} записей'"
                            :css="css.pagination_info">
                    </vuetable-pagination-info>
                </div>
                <div class="col-md-6">
                    <vuetable-pagination ref="pagination"
                                         @vuetable-pagination:change-page="onChangePage"
                                         :css="css.pagination">
                    </vuetable-pagination>
                </div>
            </div>

            <div class="row">
                <div class="col-md-3">
                    <label> По
                        <select name="perpage"
                                aria-controls="example"
                                class="form-control input-sm"
                                style="width:100px;
                                display:inline-block"
                                v-model="perPage"
                                @change="onPerPageChange">
                            <option value="10">10</option>
                            <option value="25">25</option>
                            <option value="50">50</option>
                            <option value="100">100</option>
                            <option value="999999999">Все</option>
                        </select>
                        записей на страницу
                    </label>
                </div>
            </div>

            <form method="post" action="" style="display:none" id="reportForm">
                <input type="text" name="period_start" :value="datePeriodStart">
                <input type="text" name="period_end" :value="datePeriodEnd">
                <input type="text" name="data" :value="$refs.vuetable.selectedTo" v-if="$refs.vuetable && $refs.vuetable.selectedTo.length > 0">
                {{csrf_field()}}
            </form>

        </div><!-- /.box-body -->

        <!-- Modal GROUP-->
        <div id="machine-group-add-modal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">

                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Создание новой группы</h4>
                    </div>
                    <div class="modal-body">
                        <form class="form">
                            <input type="hidden" name="_token" value="{{csrf_token()}}"/>

                            <div class="form-group">
                                <label for="name">Название группы</label>
                                <input type="text" class="form-control" id="name" aria-describedby="nameHelp" placeholder="Введите название группы"
                                       v-model="modals.machine_group.name">
                                <small id="nameHelp" class="form-text text-muted">Обычно первые буквы имени и фамилии</small>
                            </div>
                            <div class="form-group">
                                <label for="name">Локация группы</label>
                                <select type="text" class="form-control" id="location_id" placeholder="Выберите локацию"
                                       v-model="modals.machine_group.location_id">
                                    <option v-for="location in locations" :value="location.id">@{{ location.name }}</option>
                                </select>
                                <small id="nameHelp" class="form-text text-muted text-danger" v-if="modals.machine_group.showNameError">Поле название группы не может быть пустым</small>
                                <br v-if="modals.machine_group.showNameError && modals.machine_group.showLocationError">
                                <small id="nameHelp" class="form-text text-muted text-danger" v-if="modals.machine_group.showLocationError">Поле локация группы не может быть пустым</small>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Отмена</button>
                        <button type="button" class="btn btn-primary" @click="onModalMachineGroupSaveClick">Сохранить</button>
                    </div>
                </div>

            </div>
        </div>

        <!-- Modal MACHINE-->
        <div id="machine-add-modal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">

                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title" v-if="modals.machine.id === null">Добавление машины</h4>
                        <h4 class="modal-title" v-else>Редактирование машины @{{ modals.machine.name }}</h4>
                    </div>
                    <div class="modal-body">
                        <form class="form">
                            <input type="hidden" name="_token" value="{{csrf_token()}}"/>

                            <div class="form-group">
                                <label for="name">Название машины</label>
                                <input type="text" class="form-control" id="name" aria-describedby="nameHelp" placeholder="Введите название машины"
                                       v-model="modals.machine.name">
                                <small id="nameHelp" class="form-text text-muted">Обычно название группы и трёхзначный порядковый номер</small>
                            </div>

                            <!--div class="form-group">
                                <label for="name">Тип тарифкации</label>
                                <select type="text" class="form-control" id="location_id" placeholder="Выберите локацию"
                                        v-model="modals.machine.type">
                                    <option value="consumption">Потребление</option>
                                    <option value="per_month">Размещение (помесячно)</option>
                                </select>
                            </div-->

                            <div class="form-group">
                                <label for="name">Потребление Вт/ч</label>
                                <input type="number" step="1" min="1" max="999999999" class="form-control" id="consumption" aria-describedby="consumptionHelp" placeholder="Потребление машины в Вт"
                                       v-model="modals.machine.consumption">
                                <small id="consumptionHelp" class="form-text text-muted">Значение указывается в Вт</small>
                            </div>

                            <div class="form-group">
                                <label for="name">Цена размещения в месяц</label>
                                <input type="number" step="1" min="1" max="999999999" class="form-control" id="price_per_month" aria-describedby="price_per_monthHelp" placeholder="Цена месяца размещения машины"
                                       v-model="modals.machine.price_per_month">
                                <small id="price_per_monthHelp" class="form-text text-muted">Значение указывается в рублях</small>
                            </div>

                            <div class="form-group">
                                <label for="name">Заметка</label>
                                <input type="text" class="form-control" id="note" aria-describedby="noteHelp" placeholder="Дополнительная информация"
                                       v-model="modals.machine.note">
                                <small id="noteHelp" class="form-text text-muted">Дополнительная информация о машине</small>
                            </div>

                            <div class="form-group">
                                <label for="name">Начало тарифкации</label>
                                <flat-pickr v-model="modals.machine.date_start" :config="dateConfigStart" placeholder="Выбрать дату начала тарифкации..."></flat-pickr>

                                <small id="nameHelp" class="form-text text-muted">Выберите дату начала тарификации машины</small>

                                <small id="nameHelp" class="form-text text-muted text-danger" v-if="modals.machine.showNameError">
                                    <br>
                                    Поле название группы не может быть пустым
                                </small>
                            </div>

                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger pull-left" @click="onModalMachineDeleteClick"><i class="fa fa-trash"></i> Удалить</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Отмена</button>
                        <button type="button" class="btn btn-primary" @click="onModalMachineSaveClick">Сохранить</button>
                    </div>

                </div>
            </div>
        </div>

        <!-- Modal Finance-->
        <div id="finance-add-modal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">

                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Добавление финансовой операции</h4>
                    </div>
                    <div class="modal-body">
                        <form class="form">
                            <input type="hidden" name="_token" value="{{csrf_token()}}"/>

                            <div class="form-group">
                                <label for="name">Операция</label>
                                <select type="text" class="form-control" id="location_id" placeholder="Выберите действие"
                                        v-model="modals.finance.action">
                                    <option value="=">Вклад</option>
                                    <option value="-">Вычет</option>
                                </select>
                                <small id="nameHelp" class="form-text text-muted"></small>
                            </div>

                            <div class="form-group" :class='{"has-error":modals.finance.showValueError}'>
                                <label for="name">Сумма</label>
                                <input type="number" class="form-control" id="value" step='0.01' min="0" aria-describedby="valueHelp" placeholder="Сумма средств в совершенной операции"
                                       v-model="modals.finance.value">
                                <small id="valueHelp" class="form-text text-muted">Сумма вычитаемых либо вкладываемых средств. Число не меньше нуля.</small>
                            </div>

                            <div class="form-group">
                                <label for="name">Дата</label>
                                <flat-pickr v-model="modals.finance.date" :config="dateConfigStart" placeholder="Выбрать дату..."></flat-pickr>

                                <small id="nameHelp" class="form-text text-muted">Выберите дату операции</small>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger pull-left" @click="onModalFinanceDeleteClick"><i class="fa fa-trash"></i> Удалить</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Отмена</button>
                        <button type="button" class="btn btn-primary" @click="onModalFinanceSaveClick">Сохранить</button>
                    </div>
                </div>

            </div>
        </div>

    </div><!-- /.box --><!-- /clients_table -->
</div>
@endsection

@section('css')

    <style>
        th.sortable{
            cursor: pointer;
        }
    </style>

@endsection

@section('js')
    <template id="vue-table2-detail-row">
        <div>
            <dl class="dl-horizontal">

                <dt>Машины: </dt>
                <dd>
                    <div class="group-list">

                        <div v-if="rowData.machine_groups.length === 0">
                            <button class="btn btn-default btn-xs" @click.prevent="onAddMachineGroupModalClick(rowData)"><i class="fa fa-plus"></i> Создать группу</button>
                        </div>

                        <div class="group" v-for="machine_group in rowData.machine_groups">
                            <span v-for="(machine, key) in machine_group.machines">

{{--

                                <span style="margin-right:5px;"
                                      class="label label-info"
                                      :title="machine.consumption + ' Вт, ' + machine.price_per_month + ' р.'">
                                    @{{machine.name}}
                                </span>
                                <button class="btn btn-xs" style=""><i class="fa fa-cog"></i></button>

--}}

                                <button type="button"
                                        class="btn btn-labeled btn-default btn-xs"
                                        :class="{'btn-warning':(!machine.consumption && !machine.price_per_month)}"
                                        style="margin-right:5px;"
                                        :title="getMachineTitle(machine)"
                                        @click="onEditMachineModalClick(machine)">
                                    <span class="btn-label"><i class="fa fa-server"></i></span>&nbsp;
                                    @{{machine.name}}

                                </button>
                            </span>

                            <button class="btn btn-default btn-xs" @click.prevent="onAddMachineModalClick(machine_group)"><i class="fa fa-plus"></i> Добавить машину</button>
                        </div>
                    </div>
                </dd>

            </dl>
            <dl class="dl-horizontal">

                <dt><a :href="'/admin/finances?client_id='+rowData.id">Финансы:</a> </dt>
                <dd>
                    <div class="group-list">
                        <div class="group">
                            <span v-for="finance in rowData.finances">
                                <button type="button"
                                        class="btn btn-labeled btn-default btn-xs"
                                        style="margin-right:5px;"
                                        @click="onEditFinanceModalClick(finance)">
                                    <span class="btn-label">
                                        @{{ finance.action === '=' ? 'Вклад&nbsp;' : '' }}
                                        @{{ finance.action === '-' ? 'Вычет&nbsp;' : '' }}
                                        @{{ finance.date }}:&nbsp;
                                        @{{ finance.value }} <i class="fa fa-rub"></i>
                                    </span>&nbsp;


                                </button>
                            </span>

                            <button class="btn btn-default btn-xs" @click.prevent="onAddFinanceModalClick(rowData.id, sumFinances(rowData.finances))"><i class="fa fa-plus"></i> Добавить</button>

                            <span>
                                Итог: <span v-text="sumFinances(rowData.finances)"></span>
                                        <i class="fa fa-rub"></i>
                            </span>
                        </div>
                    </div>
                </dd>

            </dl>
        </div>
    </template>

    <template id="vue-table2-search-text">
        <div class="form-inline">
            <div class="col-md-5 pull-right" style="margin-bottom:5px">
                <input type="text"
                       class="form-control"
                       v-model="filterText"
                       @keyup.enter.prevent="doFilter"
                       placeholder="Имя, почта или телфон">

                <button class="btn btn-default" @click="doFilter"><i class="fa fa-search"></i> Поиск</button>
                <button class="btn btn-default" @click="resetFilter"><i class="fa fa-eraser"></i> Очистить</button>
            </div>
        </div>
    </template>

    <!--script type="text/javascript" src="https://unpkg.com/lodash"></script-->
    <script type="text/javascript" src="https://unpkg.com/lodash@4.17.4/lodash.js"></script>

    <!--script type="text/javascript" src="https://unpkg.com/vue"></script-->
    <script type="text/javascript" src="https://unpkg.com/vue@2.5.2/dist/vue.js"></script>
    <script type="text/javascript" src="https://unpkg.com/vuetable-2@1.7.4/dist/vuetable-2.js"></script>
    <script type="text/javascript" src="https://unpkg.com/flatpickr@4.2.4/dist/flatpickr.js"></script>
    <script type="text/javascript" src="https://unpkg.com/flatpickr@4.2.4/dist/l10n/ru.js"></script>
    <script type="text/javascript" src="https://unpkg.com/vue-flatpickr-component@6.2.0/dist/vue-flatpickr.min.js"></script>
    <script type="text/javascript" src="/js/vue-table2-detail-row.js"></script>
    <script type="text/javascript" src="/js/vue-table2-search-text.js"></script>
    <script type="text/javascript" src="/js/translit.js"></script>

    <script type="text/javascript">

        var preventReloadOnPopState = false;

        Vue.use(Vuetable);

        Vue.component('search-text', Vue.searchText);
        Vue.component('flat-pickr', VueFlatpickr);

        var clients_table = new Vue({
            el: '#places_list',
            components:{
                'vuetable-pagination': Vuetable.VuetablePagination,
                'vuetable-pagination-info': Vuetable.VuetablePaginationInfo,
                'vuetable-filter-bar': Vuetable.VuetableFilterBar,
                'detailRow': Vue.detailRow
            },
            data: {
                locationConsumption: {!! $locationConsumption !!},

                datePeriodStart:null,
                datePeriodEnd:null,
                dateConfigStart: {
                    altFormat: "с F j, Y",
                    altInput: true,
                    locale: 'ru'
                },
                dateConfigEnd: {
                    altFormat: "по F j, Y",
                    altInput: true,
                    locale: 'ru'
                },

                locations: {!! \App\Location::all() !!},

                modals: {
                    machine_group:{
                        name: '',
                        location_id: null,
                        client_id: null,
                        showLocationError: false,
                        showNameError: false
                    },
                    machine:{
                        id:null,
                        name: '',
                        consumption: null,
                        price_per_month: null,
                        machine_group_id: null,
                        note:null,
                        date_start: null,
                        type: 'per_month',
                        showNameError: false
                    },
                    finance: {
                        action: null,
                        value: null,
                        date: null,
                        client_id: null,
                        id:null,
                        showValueError: false
                    }
                },

                perPage: 50,
                fields: [
                    {
                        name: '__checkbox',   // <----
                        sortField: 'id',
                        title: '#',
                        titleClass: 'center aligned',
                        dataClass: 'right aligned'
                    },{
                        name: 'id',   // <----
                        sortField: 'id',
                        title: '#',
                        titleClass: 'center aligned',
                        dataClass: 'right aligned'
                    },
                    {
                        name: '__slot:name',   // <----
                        sortField: 'name',
                        title: 'Название',
                        titleClass: 'center aligned',
                        dataClass: 'right aligned'
                    },
                    {
                        name: 'phone',   // <----
                        sortField: 'phone',
                        title: 'Телефон',
                        titleClass: 'center aligned',
                        dataClass: 'right aligned'
                    },
                    {
                        name: 'email',   // <----
                        sortField: 'email',
                        title: 'Email',
                        titleClass: 'center aligned',
                        dataClass: 'right aligned'
                    },
                    {
                        name: 'info',   // <----
                        sortField: 'info',
                        title: 'Информация',
                        titleClass: 'center aligned',
                        dataClass: 'right aligned'
                    },
                    '__slot:actions'
                ],
                filter: window.location.hash.substr(1),
                searchText: '',
                css: {
                    table: {
                        tableClass: 'table table-striped table-bordered',
                        ascendingIcon: 'fa fa-chevron-up pull-right',
                        descendingIcon: 'fa fa-chevron-down pull-right',
                        handleIcon: 'fa fa-bars',
                        renderIcon: function (classes, options) {
                            return "<span class='"+classes.join(' ')+"'></span>";
                        }
                    },
                    pagination: {
                        wrapperClass: "pagination pull-right",
                        activeClass: "btn-primary",
                        disabledClass: "disabled",
                        pageClass: "btn btn-border",
                        linkClass: "btn btn-border",
                        icons: {
                            first: "",
                            prev: "",
                            next: "",
                            last: ""
                        }
                    },
                    pagination_info: {
                        infoClass: "pagination pull-left"
                    }
                }
            },
            watch: {
                perPage: function(val, oldVal) {
                    $this = this;
                    Vue.nextTick(function(){
                        $this.$refs.vuetable.refresh()
                    });
                },
            },
            computed: {},
            methods: {
                onPaginationData: function (paginationData) {
                    this.$refs.pagination.setPaginationData(paginationData);
                    this.$refs.paginationInfo.setPaginationData(paginationData)
                },

                onChangePage: function (page) {
                    hashParams['search'] = this.searchText;

                    preventReloadOnPopState = true;
                    window.location = '/admin/clients#'
                        + 'search=' + hashParams['search']
                        + '&page=' + page
                        + '&per_page=' + this.perPage ;

                    this.$refs.vuetable.changePage(page);
                },

                onPerPageChange: function () {
                    hashParams['search'] = this.searchText;

                    preventReloadOnPopState = true;
                    window.location = '/admin/clients#'
                        + 'search=' + hashParams['search']
                        + '&page=' + 1
                        + '&per_page=' + this.perPage ;

                    this.$refs.vuetable.changePage(hashParams['page']);
                },

                editRow: function(rowData){
                    window.location = '/admin/clients/' + rowData.id + '/edit';
                },

                deleteRow: function(rowData){
                    if (confirm('Вы уверены?'))
                    {
                        $.post('/api/clients/' + rowData.id + '/delete', {
                            '_token':"{{csrf_token()}}"
                        }).done(function()
                        {
                            location.reload();
                        }).fail(function()
                        {
                            alert('Перед удалением необходимо отвязать от пользователя группу машин. Сделать это можно в редактировании выбранного клиента.');
                        });
                    }
                },

                onCellClicked: function (data, field, event) {
                    this.$refs.vuetable.toggleDetailRow(data.id)
                },

                onGenerateReport: function() {
                    $('#reportForm').submit();
                },

                onModalMachineGroupSaveClick: function()
                {
                    var $this = this;

                    var errors = 0;
                    this.modals.machine_group.showLocationError = false;
                    this.modals.machine_group.showNameError = false;

                    if (this.modals.machine_group.location_id == null) {
                        this.modals.machine_group.showLocationError = true;
                        errors++;
                    }

                    if (this.modals.machine_group.name === undefined || this.modals.machine_group.name.length < 2) {
                        this.modals.machine_group.showNameError = true;
                        errors++;
                    }

                    if (errors > 0)
                        return false;

                    $.post('/api/machine_group',
                        {
                            _token:'{{csrf_token()}}',
                            name: this.modals.machine_group.name,
                            location_id: this.modals.machine_group.location_id,
                            client_id: this.modals.machine_group.client_id
                        }).done(function(){
                            $('#machine-group-add-modal').modal('hide');

                            Vue.nextTick(function(){
                                $this.$refs.vuetable.refresh()
                            });
                        });
                },
                onModalMachineSaveClick: function()
                {
                    var $this = this;

                    this.modals.machine_group.showNameError = false;

                    if (this.modals.machine.name === undefined || this.modals.machine.name.length < 2) {
                        this.modals.machine.showNameError = true;
                        return false;
                    }

                    // ОБНУЛЯТЬ ЗНАЧЕНИЕ ТОГО ТИПА ТАРИФИКАЦИИ КОТОРЫЙ НЕ ВЫБРАН!

                    $.post('/api/machine',
                        {
                            _token:'{{csrf_token()}}',
                            id: this.modals.machine.id,
                            name: this.modals.machine.name,
                            consumption: this.modals.machine.consumption,
                            price_per_month: this.modals.machine.price_per_month,
                            machine_group_id: this.modals.machine.machine_group_id,
                            note: this.modals.machine.note,
                            date_start: this.modals.machine.date_start
                        }).done(function(){
                            $('#machine-add-modal').modal('hide');

                            Vue.nextTick(function(){
                                $this.$refs.vuetable.refresh()
                            });
                        });
                },
                onModalMachineDeleteClick: function()
                {
                    $this = this;

                    if (!confirm('Требуется подтверждение'))
                        return;

                    $.ajax({
                        url: '/api/machine',
                        data: {
                            _token:'{{csrf_token()}}',
                            id: this.modals.machine.id
                        },
                        type: 'DELETE',
                        success: function(result) {
                            $('#machine-add-modal').modal('hide');

                            Vue.nextTick(function(){
                                $this.$refs.vuetable.refresh()
                            });
                        }
                    });
                },

                onModalFinanceSaveClick: function()
                {
                    var $this = this;

                    this.modals.finance.showValueError = false;

                    if (this.modals.finance.value === undefined || this.modals.finance.value === null) {
                        this.modals.finance.showValueError = true;
                        return false;
                    }

                    $.post('/api/finance',
                        {
                            _token:'{{csrf_token()}}',
                            id: this.modals.finance.id,
                            action: this.modals.finance.action,
                            value: this.modals.finance.value,
                            client_id: this.modals.finance.client_id,
                            date: this.modals.finance.date
                        }).done(function(){
                            $('#finance-add-modal').modal('hide');

                            Vue.nextTick(function(){
                                $this.$refs.vuetable.refresh()
                            });
                        });
                },
                onModalFinanceDeleteClick: function()
                {
                    $this = this;

                    if (!confirm('Требуется подтверждение'))
                        return;

                    $.ajax({
                        url: '/api/finance',
                        data: {
                            _token:'{{csrf_token()}}',
                            id: this.modals.finance.id
                        },
                        type: 'DELETE',
                        success: function(result) {
                            $('#finance-add-modal').modal('hide');

                            Vue.nextTick(function(){
                                $this.$refs.vuetable.refresh()
                            });
                        }
                    });
                }
            },
            mounted: function () {
                var $this = this;

                (function autoLoop () {
                    setTimeout(function () {
                        if ($this.$refs.vuetable.tablePagination == null)
                            autoLoop(i);
                        else {
                            clients_table.perPage = parseInt(hashParams['per_page']);

                            Vue.nextTick(function(){
                                clients_table.$refs.vuetable.gotoPage(hashParams['page']);
                            });
                        }
                    }, 200)
                })();
            }
        });

        var hashParams = {};
        var URLHash = window.location.hash.substr(1).split('&');
        for (i = 0; i < URLHash.length; i++) {
            var tmp = URLHash[i].split('=');
            hashParams[tmp[0]] = tmp[1];
        }

        $(function () {

            clients_table.$refs.searchtext.filterText = hashParams['search'];

            var page = hashParams['page'];
/*
            function getFilteredPlaces() {

                preventReloadOnPopState = true;

                window.location = '/admin/clients#'
                    + 'search=' + clients_table.searchText
                    + '&page=' + page
                    + '&per_page=' + clients_table.perPage ;

                location.reload();
            }
*/
            window.onpopstate = function(event) {
                if (!preventReloadOnPopState)
                    location.reload();

                preventReloadOnPopState = false;
            };


        });
    </script>
@endsection
