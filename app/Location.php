<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    public function machine_groups()
    {
        return $this->hasMany(MachineGroup::class);
    }
}
