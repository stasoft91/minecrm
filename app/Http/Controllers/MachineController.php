<?php

namespace App\Http\Controllers;

use App\Machine;
use App\MachineGroup;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class MachineController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create (Request $request)
    {
        $id = $request->get('id');

        $m = isset($id) ? Machine::find($request->get('id')) : new Machine();
        $m->name = $request->name;
        $m->consumption = $request->consumption ?? 0;
        $m->price_per_month = $request->price_per_month ?? 0;
        $m->note = $request->note ?? '';
        $m->date_start = $request->date_start ?? Carbon::now();
        $m->machine_group_id = $request->machine_group_id;
        $m->save();

        return new JsonResponse('true', 200);
    }

    public function delete (Request $request)
    {
        $id = $request->get('id');

        Machine::find($id)->delete();

        return new JsonResponse('true', 200);
    }
}
