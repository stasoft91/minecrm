<?php

use SleepingOwl\Admin\Navigation\Page;

// Default check access logic
// AdminNavigation::setAccessLogic(function(Page $page) {
// 	   return auth()->user()->isSuperAdmin();
// });
//
// AdminNavigation::addPage(\App\User::class)->setTitle('test')->setPages(function(Page $page) {
// 	  $page
//		  ->addPage()
//	  	  ->setTitle('Dashboard')
//		  ->setUrl(route('admin.dashboard'))
//		  ->setPriority(100);
//
//	  $page->addPage(\App\User::class);
// });
//
// // or
//
// AdminSection::addMenuPage(\App\User::class)

return [

    [
        'title' => 'Главная',
        'icon'  => 'fa fa-dashboard',
        'url'   => route('admin.dashboard'),
    ],
/*
    [
        'title' => 'Information',
        'icon'  => 'fa fa-exclamation-circle',
        'url'   => route('admin.information'),
    ],
*/
    [
        'title' => 'Клиенты',
        'icon'  => 'fa fa-users',
        'url'   => '/admin/clients#search=&page=1&per_page=50',
    ],
    [
        'title' => 'Настройки',
        'icon'  => 'fa fa-cog',
        'pages' => [
            (new Page(\App\Location::class))
                ->setPriority(10)
                ->setIcon('fa fa-industry')
                ->setAccessLogic(function (Page $page) {
                    return auth()->user()->hasRole('admin');
                }),
            (new Page(\App\MachineGroup::class))
                ->setPriority(20)
                ->setIcon('fa fa-object-group')
                ->setAccessLogic(function (Page $page) {
                    return auth()->user()->hasRole('admin');
                }),
            (new Page(\App\Machine::class))
                ->setPriority(20)
                ->setIcon('fa fa-server')
                ->setAccessLogic(function (Page $page) {
                    return auth()->user()->hasRole('admin');
                }),
            (new Page(\App\Finance::class))
                ->setPriority(90)
                ->setIcon('fa fa-rub')
                ->setAccessLogic(function (Page $page) {
                    return auth()->user()->hasRole('admin');
                }),
        ]
    ],
    [
        'title' => 'Пользователи CRM',
        'icon'  => 'fa fa-id-card',
        'pages' => [
            (new Page(\App\User::class))
                ->setPriority(100)
                ->setIcon('fa fa-id-card')
                ->setAccessLogic(function (Page $page) {
                    return auth()->user()->hasRole('admin');
                }),
            (new Page(\App\Role::class))
                ->setPriority(100)
                ->setIcon('fa fa-id-badge')
                ->setAccessLogic(function (Page $page) {
                    return auth()->user()->hasRole('admin');
                }),
        ]
    ],
    [
        'title' => 'Выход',
        'icon'  => 'fa fa-sign-out',
        'url'   => '/logout',
    ],
];

    //
    //        // or
    //
    //        new Page([
    //            'title'    => 'News',
    //            'priority' => 200,
    //            'model'    => \App\News::class
    //        ]),
    //
    //        // or
    //        (new Page(/* ... */))->setPages(function (Page $page) {
    //            $page->addPage([
    //                'title'    => 'Blog',
    //                'priority' => 100,
    //                'model'    => \App\Blog::class
	//		      ));
    //
	//		      $page->addPage(\App\Blog::class);
    //	      }),
    //
    //        // or
    //
    //        [
    //            'title'       => 'News',
    //            'priority'    => 300,
    //            'accessLogic' => function ($page) {
    //                return $page->isActive();
    //		      },
    //            'pages'       => [
    //
    //                // ...
    //
    //            ]
    //        ]
    //    ]
     //]
//];