<div class="row">
    <div class="col-md-6 col-sm-6 col-xs-12">
        <a href="/admin/clients">
            <div class="info-box">
                <span class="info-box-icon bg-aqua"><i class="fa fa-users"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Клиентов</span>
                    <span class="info-box-number">{{\App\Client::count()}}</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </a>
    </div>
    <!-- /.col -->
    <div class="col-md-6 col-sm-6 col-xs-12">
        <a href="/admin/machines">
            <div class="info-box">
                <span class="info-box-icon bg-red"><i class="fa fa-server"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Машин</span>
                    <span class="info-box-number">{{\App\Machine::count()}}</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </a>
    </div>
</div>
<div class="row">
    <div class="col-md-6 col-sm-6 col-xs-12">
        <a href="/admin/clients">
            <div class="info-box">
                <span class="info-box-icon bg-yellow"><i class="fa fa-dashboard"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Суммарное потребление всех объектов</span>
                    <span class="info-box-number">{{$total_consumption}} кВт</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </a>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Статистика</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <p class="text-center">
                            <strong>Потребление, Вт</strong>
                        </p>

                        <div class="chart" id="line-chart" style="height: 300px;"></div>

                        <!-- /.chart-responsive -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- ./box-body --> {{--
            <div class="box-footer">
                <div class="row">
                    <div class="col-sm-3 col-xs-6">
                        <div class="description-block border-right">
                            <span class="description-percentage text-green"><i class="fa fa-caret-up"></i> 17%</span>
                            <h5 class="description-header">$35,210.43</h5>
                            <span class="description-text">Потребление</span>
                        </div>
                        <!-- /.description-block -->
                    </div>
                    <!-- /.col -->
                    <div class="col-sm-3 col-xs-6">
                        <div class="description-block border-right">
                            <span class="description-percentage text-yellow"><i class="fa fa-caret-left"></i> 0%</span>
                            <h5 class="description-header">$10,390.90</h5>
                            <span class="description-text">TOTAL COST</span>
                        </div>
                        <!-- /.description-block -->
                    </div>
                    <!-- /.col -->
                    <div class="col-sm-3 col-xs-6">
                        <div class="description-block border-right">
                            <span class="description-percentage text-green"><i class="fa fa-caret-up"></i> 20%</span>
                            <h5 class="description-header">$24,813.53</h5>
                            <span class="description-text">TOTAL PROFIT</span>
                        </div>
                        <!-- /.description-block -->
                    </div>
                    <!-- /.col -->
                    <div class="col-sm-3 col-xs-6">
                        <div class="description-block">
                            <span class="description-percentage text-red"><i class="fa fa-caret-down"></i> 18%</span>
                            <h5 class="description-header">1200</h5>
                            <span class="description-text">GOAL COMPLETIONS</span>
                        </div>
                        <!-- /.description-block -->
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.box-footer -->
            --}}
        </div>
        <!-- /.box -->
    </div>
    <!-- /.col -->
</div>

<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script>

<script>
    $(function () {
        "use strict";

        function getRandomColor() {
            var result = [];

            for (var locations= 0; locations < {{\App\Location::count()}}; locations++)
            {
                var letters = '0123456789ABCDEF';
                var color = '#';
                for (var i = 0; i < 6; i++) {
                    color += letters[Math.floor(Math.random() * 16)];
                }
                result[result.length] = color;
            }

            return result;
        }

        // LINE CHART
        var line = new Morris.Line({
            element: 'line-chart',
            resize: true,
            data: {!! $consumption !!},
            xkey: 'date',
            ykeys: {!! \App\Location::pluck('name') !!},
            labels: {!! \App\Location::pluck('name') !!},
            colors: getRandomColor(),
            hideHover: 'auto',
            smooth: false
        });
    });

</script>