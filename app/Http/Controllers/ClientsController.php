<?php

namespace App\Http\Controllers;

use App\Client;
use App\Location;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ClientsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function viewIndex()
    {
        $locationConsumption = collect();
        foreach(Location::get() as $location)
        {
            $locationTotal = 0;
            foreach ($location->machine_groups as $machine_group)
                $locationTotal += $machine_group->machines()->sum('consumption');

            $locationConsumption->put($location->name, $locationTotal);
        }

        return view('clients.table', compact('locationConsumption'));
    }

    public function apiClientList(Request $request)
    {
        $limit = $request->get('per_page');

        $offset = ($request->get('page') - 1) * $limit;

        $paginate = Client::paginate();

        $select =  Client::with(['machine_groups.machines', 'finances'])->limit($limit)->offset($offset);

        $selectedNoLimitCount = Client::with(['machine_groups.machines']);

        $search = $request->get('search');
        if ($search) {
            $select = $select->where(function ($q) use ($search) {
                $q->where('name', 'LIKE', '%' . $search . '%')
                    ->orWhere('phone', 'LIKE', '%' . $search . '%')
                    ->orWhere('email', 'LIKE', '%' . $search . '%');
            });

            $selectedNoLimitCount = $selectedNoLimitCount->where(function ($q) use ($search) {
                $q->where('name', 'LIKE', '%' . $search . '%')
                    ->orWhere('phone', 'LIKE', '%' . $search . '%')
                    ->orWhere('email', 'LIKE', '%' . $search . '%');
            });
        }

        if ($request->get('sort'))
        {
            $order = explode( '|', $request->get('sort'));
            $select = $select->clearOrdersBy()->orderBy($order[0], $order[1]);
        }

        $selectedNoLimitCount = $selectedNoLimitCount->count();

        $result = collect();
        $result->put('data', $select->get() );

        $result->put('current_page', $paginate->currentPage());
        $result->put('from', $offset + 1);
        $result->put('per_page', $paginate->perPage());
        $result->put('to', min($offset + $limit, $selectedNoLimitCount));
        $result->put('last_page', ceil($selectedNoLimitCount/$limit));
        $result->put('total', $selectedNoLimitCount);
        $result->put('path', '/api_p/places_list');
        $result->put('next_page_url', $paginate->url($paginate->currentPage()+1));
        $result->put('prev_page_url', $paginate->url($paginate->currentPage()-1));

        return $result;
    }

    public function generate(Request $request)
    {
        $data = $request->get('data');
        $period_start = $request->get('period_start') ? Carbon::createFromFormat('Y-m-d', $request->get('period_start')) : Carbon::now()->startOfMonth();
        $period_end = $request->get('period_end') ? Carbon::createFromFormat('Y-m-d', $request->get('period_end')) : Carbon::now();

        $clientResults = collect();

        if (!$data) {
            foreach (Client::get() as $client) {
                $clientResults->push($this->getClientReport($client, $period_start, $period_end));
            }
        }
        else {
            foreach (explode(',', $data) as $clientId) {
                $client = Client::find($clientId);
                $clientResults->push($this->getClientReport($client, $period_start, $period_end));
            }
        }

        return Excel::create("Report_{$period_start->format('Y-m-d')}-{$period_end->format('Y-m-d')}", function($excel) use ($clientResults, $period_start, $period_end) {

            // Set the title
            $excel->setTitle('Отчёт с '. $period_start->format('d/m/Y') . ' по ' . $period_end->format('d/m/Y'));

            // Chain the setters
            $excel->setCreator('MineCRM')
                ->setCompany('');

            // Call them separately
            $excel->setDescription('Отчёт');

            $rowCounter = 0;
            $excel->sheet('Report', function($sheet) use ($clientResults, $rowCounter, $period_start, $period_end) {
                $sheet->appendRow([
                    'с ' . $period_start->format('d/m/Y') . ' по ' . $period_end->format('d/m/Y'),
                    'Дней в периоде: ' . $period_end->diff($period_start)->days
                ]);
                $rowCounter++;

                $sheet->appendRow([
                    null,
                ]);
                $rowCounter++;

                $sheet->appendRow([
                    'Имя',                          //A
                    'Машина',                       //B
                    'Потребление Вт\ч',             //C
                    'Стоимость кВт\ч в локации',    //D
                    'Отработано дней в периоде',    //E
                    'Итог за период',               //F
                    'Цена размещения за период'     //G
                ]);
                $rowCounter++;

                foreach ($clientResults as $client) {
                    $sheet->appendRow([
                        $client['client']->name,
                    ]);
                    $rowCounter++;

                    $clientByMonthTotalCost = 0;

                    foreach($client['machines'] as $machine) {
                        $price_per_day_fixed = $machine['machine']->price_per_month/$period_start->daysInMonth;
                        $clientByMonthCost = $price_per_day_fixed * $machine['machine_days'];
                        $clientByMonthTotalCost += $clientByMonthCost;

                        $sheet->appendRow([
                            null,                                   //A
                            $machine['machine']->name,              //B
                            $machine['machine']->consumption,       //C
                            $machine['location']->price,            //D
                            $machine['machine_days'],               //E
                            number_format($machine['cost'], 2) . ' р.',  //F
                            number_format($clientByMonthCost, 2) . ' р.' //G
                        ]);
                        $rowCounter++;

                        $sheet->cell('C'.($rowCounter), function($cell) {
                            $cell->setAlignment('left');
                        });
                        $sheet->cell('D'.($rowCounter), function($cell) {
                            $cell->setAlignment('left');
                        });
                        $sheet->cell('E'.($rowCounter), function($cell) {
                            $cell->setAlignment('left');
                        });
                    }

                    $sheet->appendRow([
                        null, //A
                        null, //B
                        null, //C
                        null, //D
                        'Сумма: ', //E
                        number_format($client['total_cost'], 2) . ' р.', //F
                        number_format($clientByMonthTotalCost, 2) . ' р.', //G
                    ]);
                    $rowCounter++;
                    $sheet->cell('E'.($rowCounter), function($cell) {
                        $cell->setAlignment('right');
                    });
                    $sheet->cell('F'.($rowCounter), function($cell) {
                        $cell->setFont([
                            'bold'       =>  true
                        ]);
                    });
                    $sheet->cell('G'.($rowCounter), function($cell) {
                        $cell->setFont([
                            'bold'       =>  true
                        ]);
                    });
                    $sheet->appendRow([
                        null,
                        null,
                        null,
                        null,
                        null,
                    ]);
                    $rowCounter++;
                }
            });

        })->download();
    }

    /**
     * @param $client_id
     * @throws
     */
    public function delete ($client_id)
    {
        Client::find($client_id)->delete();
    }

    protected function getClientReport(Client $client, Carbon $period_start, Carbon $period_end)
    {
        $clientSubResult = collect();
        $totalClientCost = 0;

        $machines = collect();

        foreach ($client->machine_groups as $machine_group) {
            $machinePrice = $machine_group->location->price;

            foreach ($machine_group->machines as $machine)
            {
                $machineResult = collect();
                if ($period_end->lt($machine->date_start))
                {
                    // машина начала работу до конца выбранного периода
                    continue;
                }

                if ($period_start->lt($machine->date_start)){
                    // машина начала работать в середине периода выбранного периода
                    $machineDays = $period_end->diff($machine->date_start)->days;
                }
                else {
                    $machineDays = $period_end->diff($period_start)->days;
                }

                $machineHours = $machineDays * 24;

                //// (ватт за месяц)/1000 = киловатт за месяц
                $machineCost = ($machineHours * $machine->consumption)/1000 * $machinePrice;

                $totalClientCost += $machineCost;

                $machineResult->put('machine', $machine);
                $machineResult->put('location', $machine_group->location);

                $machineResult->put('cost', $machineCost);
                $machineResult->put('machine_days', $machineDays);

                $machines->push($machineResult);
            }
        }

        $clientSubResult->put('client', $client);
        $clientSubResult->put('total_cost', $totalClientCost);
        $clientSubResult->put('machines', $machines);

        return $clientSubResult;
    }
}
