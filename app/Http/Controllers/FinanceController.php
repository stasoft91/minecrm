<?php

namespace App\Http\Controllers;

use App\Finance;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class FinanceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create (Request $request)
    {
        $id = $request->get('id');

        $m = isset($id) ? Finance::find($request->get('id')) : new Finance();
        $m->action = $request->action;
        $m->value = $request->value;
        $m->date = $request->date ?? Carbon::now();
        $m->client_id = $request->client_id;
        $m->save();

        return new JsonResponse('true', 200);
    }

    public function delete (Request $request)
    {
        $id = $request->get('id');

        Finance::find($id)->delete();

        return new JsonResponse('true', 200);
    }
}
