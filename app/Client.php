<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    public function machine_groups()
    {
        return $this->belongsToMany(MachineGroup::class);
    }

    public function finances()
    {
        return $this->hasMany(Finance::class);
    }
}
