<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MachineGroup extends Model
{
    public function machines()
    {
        return $this->hasMany(Machine::class);
    }

    public function location()
    {
        return $this->belongsTo(Location::class);
    }

    public function clients()
    {
        return $this->belongsToMany(Location::class);
    }
}
