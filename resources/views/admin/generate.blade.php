<html>
    <head>
        <script type="application/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <script type="application/javascript" src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
        <script type="application/javascript" src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.min.js"></script>
        <script type="application/javascript" src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.3/js/bootstrap-datetimepicker.min.js"></script>

        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.3/css/bootstrap-datetimepicker.min.css">
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

    </head>
    <body>
        <div class="container">
            <div class="row">

            </div>
            <div class="row">
                <div class='col-sm-3'>
                    <div class="form-group">
                        <label for="datetimepicker1">Начало периода</label>

                        <div class='input-group date' id='datetimepicker1'>
                            <input type='text' class="form-control" />
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class='col-sm-3'>
                    <div class="form-group">
                        <label for="datetimepicker2">Начало периода</label>

                        <div class='input-group date' id='datetimepicker2'>
                            <input type='text' class="form-control"/>
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class='col-sm-3'>
                    <small>Все даты указываются включительно</small>
                </div>
            </div>

            <div class="row">
                <div class='col-sm-3'>
                    {{$client->name}}
                </div>
            </div>
        </div>
        <script>
            $(function () {
                var bindDatePicker = function() {
                    moment.locale('ru');

                    $(".date").datetimepicker({
                        format:'DD-MM-YYYY',
                        icons: {
                            time: "fa fa-clock-o",
                            date: "fa fa-calendar",
                            up: "fa fa-arrow-up",
                            down: "fa fa-arrow-down"
                        },
                        pickTime: false,
                        locale: 'ru',
                        language: 'ru'
                    }).find('input:first').on("blur",function () {
                        // check if the date is correct. We can accept dd-mm-yyyy and yyyy-mm-dd.
                        // update the format if it's yyyy-mm-dd
                        var date = parseDate($(this).val());

                        if (! isValidDate(date)) {
                            //create date based on momentjs (we have that)
                            date = moment().format('DD-MM-YYYY');
                        }

                        $(this).val(date);
                    });
                }

                var isValidDate = function(value, format) {
                    format = format || false;
                    // lets parse the date to the best of our knowledge
                    if (format) {
                        value = parseDate(value);
                    }

                    var timestamp = Date.parse(value);

                    return isNaN(timestamp) == false;
                }

                var parseDate = function(value) {
                    var m = value.match(/^(\d{1,2})(\/|-)?(\d{1,2})(\/|-)?(\d{4})$/);
                    if (m)
                        value = m[5] + '-' + ("00" + m[3]).slice(-2) + '-' + ("00" + m[1]).slice(-2);

                    return value;
                }

                bindDatePicker();
            });
        </script>
    </body>
</html>