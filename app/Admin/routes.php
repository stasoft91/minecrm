<?php

Route::get('', ['as' => 'admin.dashboard', function () {
    $machines = \App\Machine::all();
    $consumption = collect();

    foreach($machines as $machine)
    {
        $to_push = collect();
        $to_push->put('date', $machine->date_start->format('Y-m-d'));

        foreach(\App\Location::get() as $location)
        {
            $sumConsumptionToDate = \App\Machine::where('date_start', '<=', $machine->date_start)
                ->whereHas('machine_group', function($q) use ($location)
                {
                    return $q->where('location_id', $location->id);
                })->get()->sum('consumption');

            $to_push->put($location->name, $sumConsumptionToDate);
        }
        $consumption->push($to_push);

    }

    $total_consumption = \App\Machine::whereHas('machine_group',function($q){
            $q->whereHas('location');
        })->sum('consumption')/1000;

	$content = view('admin.dashboard', compact('consumption', 'total_consumption'));
	return AdminSection::view($content, 'Главная');
}]);

Route::get('information', ['as' => 'admin.information', function () {
	$content = 'Define your information here.';
	return AdminSection::view($content, 'Information');
}]);