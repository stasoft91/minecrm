<?php

namespace App\Providers;

use SleepingOwl\Admin\Providers\AdminSectionsServiceProvider as ServiceProvider;

class AdminSectionsServiceProvider extends ServiceProvider
{

    /**
     * @var array
     */
    protected $sections = [
        \App\User::class => 'App\Admin\Sections\Users',
        \App\Role::class => 'App\Admin\Sections\Roles',

        \App\Location::class => 'App\Admin\Sections\Locations',
        \App\Machine::class => 'App\Admin\Sections\Machines',
        \App\Client::class => 'App\Admin\Sections\Clients',
        \App\MachineGroup::class => 'App\Admin\Sections\MachineGroups',
        \App\Finance::class => 'App\Admin\Sections\Finances',
    ];

    /**
     * Register sections.
     *
     * @return void
     */
    public function boot(\SleepingOwl\Admin\Admin $admin)
    {
    	//

        parent::boot($admin);
    }
}
