<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserAdmin extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $role = new \App\Role();
        $role->name = 'admin';
        $role->display_name = 'Админ';
        $role->description = 'Админ';
        $role->save();

        $user = new \App\User();
        $user->name = 'Admin';
        $user->email = 'admin@admin.com';
        $user->password = 'admin';
        $user->save();

        $user->attachRole($role);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
